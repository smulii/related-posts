<?php
/**
 * Plugin Name:       Related posts
 * Description:       Plugin for displaying user selected related posts as a teaser grid
 * Requires at least: 5.8
 * Requires PHP:      7.0
 * Version:           1.0.0
 * Author:            Samuli Martikainen
 * License:           GPL-2.0-or-later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       related-posts
 *
 * @package           related-posts
 */

function related_posts_render_callback($block_attributes, $content) {
  $wrapper_attributes = get_block_wrapper_attributes();
  $selectedPosts = $block_attributes['selectedPosts'];

  ob_start();
  ?>
  
  <div <?php echo $wrapper_attributes; ?>>
    <?php if (!empty($selectedPosts)) : ?>
      <div class="related-posts">
        <div class="related-posts__container">
          <?php foreach ($selectedPosts as $selectedPost) : ?>
            <?php
              $post_id = $selectedPost['id'];
              $thumbnail = get_the_post_thumbnail($post_id, 'large');
              $title = get_the_title($post_id);
              $date = get_the_date('j.n.Y', $post_id);
              $categories = get_the_category($post_id);
              $permalink = get_the_permalink($post_id);
            ?>

            <article class="related-posts__teaser">
              <a href="<?php echo $permalink; ?>" class="related-posts__teaser__link" aria-label="<?php echo __('Go to post:') . ' ' . $title; ?>"></a>
              
              <div class="related-posts__teaser__container">
                <?php if (!empty($thumbnail)) : ?>
                  <div class="related-posts__teaser__media">
                    <?php echo $thumbnail; ?>
                  </div>
                <?php endif; ?>

                <div class="related-posts__teaser__content">
                  <div class="related-posts__teaser__content__inner">
                    <span class="related-posts__teaser__title"><?php echo $title; ?></span>
                    <span class="related-posts__teaser__date"><?php echo $date; ?></span>
                  </div>

                  <div class="related-posts__teaser__content__footer">
                    <?php if (!empty($categories)) : ?>
                      <div class="related-posts__teaser__categories">
                        <?php foreach ($categories as $term) : ?>
                          <a class="related-posts__teaser__term" href="<?php echo get_term_link($term->term_id, 'category'); ?>" aria-label="<?php echo __('Go to category archive:') . ' ' . $term->name; ?>">
                            <?php echo $term->name; ?>
                          </a>
                        <?php endforeach; ?>
                      </div>
                    <?php endif; ?>

                    <div class="related-posts__teaser__read-more">
                      <span><?php echo __('Read more');?></span>
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                        <path d="M13.92 12 8.97 7.05l1.41-1.41L16.75 12l-6.37 6.36-1.41-1.41L13.92 12Z"/>
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
            </article>
          <?php endforeach; ?>
        </div>
      </div>
    <?php elseif (defined('REST_REQUEST') && REST_REQUEST) : ?>
      <div class="related-posts related-posts--no-posts">
        <span class="related-posts__title">Related posts</span>
        <span class="related-posts__notice"><?php echo __('No posts selected'); ?></span>
      </div>
    <?php endif; ?>
  </div>
  
  <?php
  $result = ob_get_clean();

  return $result;
}

/**
 * Registers the block using the metadata loaded from the `block.json` file.
 * Behind the scenes, it registers also all assets so they can be enqueued
 * through the block editor in the corresponding context.
 *
 * @see https://developer.wordpress.org/reference/functions/register_block_type/
 */
add_action('init', function() {
  $asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php');

  wp_register_script(
      'related-posts',
      plugins_url( 'build/index.js', __FILE__ ),
      $asset_file['dependencies'],
      $asset_file['version']
  );

  register_block_type( __DIR__ . '/build', [
    'api_version' => 2,
    'editor_script' => 'related-posts',
    'render_callback' => 'related_posts_render_callback'
  ]);
});

/**
 * If theme has allowed blocks restrictions -> add block to allowed block types
 */
add_filter('allowed_block_types_all', function($allowed_block_types, $post) {

  if (!in_array('related-posts/related-posts', $allowed_block_types)) {
    $allowed_block_types[] = 'related-posts/related-posts';
  }
  return $allowed_block_types;
  
}, 11, 2);