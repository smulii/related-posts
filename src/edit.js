/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-i18n/
 */
import { __ } from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/packages/packages-block-editor/#useBlockProps
 */
import { useBlockProps } from '@wordpress/block-editor';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

/**
 * Custom react hook for retrieving props from registered selectors.
 * 
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-data/#useselect
 */
import { useSelect } from '@wordpress/data';

/**
 * Components
 */
import ServerSideRender from '@wordpress/server-side-render';
import {
  __experimentalItemGroup as ItemGroup,
  __experimentalItem as Item,
} from '@wordpress/components';

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/developers/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({ attributes, setAttributes, isSelected }) {
  const blockProps = useBlockProps();
  const posts = useSelect((select) => {
      const queryArgs = {
        per_page: -1,
        orderby: 'title',
        order: 'asc',
        status: 'publish',
        _fields: 'id,title'
      };
      return select('core').getEntityRecords('postType', 'post', queryArgs );
  }, []);

  const unselectedPosts = posts && posts.length ? posts.filter(x => {
    if (attributes.selectedPosts && attributes.selectedPosts.length) {
      if (attributes.selectedPosts.findIndex((selectedPost) => selectedPost.id == x.id) == -1) {
        return x;
      }
    } else {
      return x;
    }
  }) : [];

  const unselectedPostItems = unselectedPosts && unselectedPosts.length ? unselectedPosts.map(x => ({ id: x.id, title: x.title.rendered })) : [];

  return (
    <div { 	...blockProps }>
      { isSelected ?
        <div class="related-posts-editor">
          <div>
            <span>{ __('Select posts') }</span>

            <ItemGroup size="small" class="related-posts-editor__item-group">
              { unselectedPostItems && unselectedPostItems.length ? unselectedPostItems.map(x => {
                return (
                  <Item class="related-posts-editor__item" onClick = { 
                    () => { 
                      setAttributes({ selectedPosts: [...attributes.selectedPosts, x] }); 
                    } 
                  }> 
                    <span>{ x.title }</span> 
                  </Item>
                );
              }) : <Item class="related-posts-editor__item related-posts-editor__item--no-posts">{ __('No posts') }</Item> }
            </ItemGroup>
          </div>
          
          <div>
            <span>{ __('Related posts') }</span>

            <ItemGroup size="small" class="related-posts-editor__item-group">
              { attributes.selectedPosts && attributes.selectedPosts.length ? attributes.selectedPosts.map(x => {
                return (
                  <Item class="related-posts-editor__item">
                    <button onClick = { 
                      () => { 
                        const newSelectedPosts = attributes.selectedPosts.filter((selectedPost) => { return selectedPost.id != x.id; });
                        setAttributes({ selectedPosts: newSelectedPosts });
                      } 
                    }>
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 461 461">
                        <path d="M285 230 456 59c6-6 6-16 0-22L424 5a16 16 0 0 0-22 0L230 176 59 5a16 16 0 0 0-22 0L5 37c-7 6-7 16 0 22l171 171L5 402c-6 6-6 15 0 21l32 33a16 16 0 0 0 22 0l171-171 172 171a16 16 0 0 0 21 0l33-33c6-6 6-15 0-21L285 230z"/>
                      </svg>
                    </button>

                    <span>{ x.title }</span>
                  </Item>
                );
              }) : <Item class="related-posts-editor__item related-posts-editor__item--no-posts">{ __('No posts') }</Item> }
            </ItemGroup>
          </div>
        </div>
      :              
        <ServerSideRender block="related-posts/related-posts" attributes={ attributes } />
      }
    </div>
  );
}
