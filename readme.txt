=== Related posts ===
Contributors:      Samuli Martikainen
Tags:              block
Tested up to:      5.9
Stable tag:        1.0.0
License:           GPL-2.0-or-later
License URI:       https://www.gnu.org/licenses/gpl-2.0.html

Plugin for displaying user selected related posts as a teaser grid

== Installation ==

1. Install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress

== Changelog ==

= 1.0.0 =
* Release